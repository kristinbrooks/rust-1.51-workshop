fn main() {
    // Option 1: Accept &Vec<i64> instead of Vec<i64>
    let numbers_opt1 = vec![2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24];

    let sum_of_nums_opt1 = sum_opt1(&numbers_opt1);
    let product_of_nums_opt1 = product_opt1(&numbers_opt1);
    let average_of_nums_opt1 = average_opt1(&numbers_opt1);

    println!("Sum of these numbers: {}", sum_of_nums_opt1);
    println!("Product of these numbers: {}", product_of_nums_opt1);
    println!("Average of these numbers: {}", average_of_nums_opt1);

    // Option 2: Accept a slice - that is, &[i64] - instead of a Vec
    let numbers_opt2 = vec![2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24];

    let sum_of_nums_opt2 = sum_opt2(&numbers_opt2);
    let product_of_nums_opt2 = product_opt2(&numbers_opt2);
    let average_of_nums_opt2 = average_opt2(&numbers_opt2);

    println!("Sum of these numbers: {}", sum_of_nums_opt2);
    println!("Product of these numbers: {}", product_of_nums_opt2);
    println!("Average of these numbers: {}", average_of_nums_opt2);
}

fn sum_opt1(numbers: &Vec<i64>) -> i64 {
    let mut total = 0;

    for num in numbers.iter() {
        total += num;
    }

    total
}

fn product_opt1(numbers: &Vec<i64>) -> i64 {
    let mut total = 1;

    for num in numbers.iter() {
        total *= num;
    }

    total
}

fn average_opt1(numbers: &Vec<i64>) -> i64 {
    let length = numbers.len() as i64;

    sum_opt1(numbers) / length
}

fn sum_opt2(numbers: &[i64]) -> i64 {
    let mut total = 0;

    for num in numbers.iter() {
        total += num;
    }

    total
}

fn product_opt2(numbers: &[i64]) -> i64 {
    let mut total = 1;

    for num in numbers.iter() {
        total *= num;
    }

    total
}

fn average_opt2(numbers: &[i64]) -> i64 {
    let length = numbers.len() as i64;

    sum_opt2(numbers) / length
}
